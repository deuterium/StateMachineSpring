package net.milanstojkovic.statemachine;

import net.milanstojkovic.statemachine.enums.Events;
import net.milanstojkovic.statemachine.enums.States;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatemachineApplicationTests {

	@Autowired
	private StateMachineFactory<States, Events> stateMachineFactory;
	private StateMachine<States, Events> stateMachine;

	@Before
	public void setUp(){
		stateMachine = stateMachineFactory.getStateMachine();
	}

	@Test
	public void testInit() {
		Assertions.assertThat(stateMachineFactory).isNotNull();
		Assertions.assertThat(stateMachine).isNotNull();
		Assertions.assertThat(stateMachine.getState().getId()).isEqualTo(States.START);
	}

	@Test
	public void testEvents() {
		stateMachine.sendEvent(Events.REGISTER);
		stateMachine.sendEvent(Events.UPLOADING);
		stateMachine.sendEvent(Events.FINISH);

		Assertions.assertThat(stateMachine.getState().getId()).isEqualTo(States.END);
	}

	@Test
	public void testCancelEvents() {
		stateMachine.sendEvent(Events.REGISTER);
		stateMachine.sendEvent(Events.UPLOADING);
		stateMachine.sendEvent(Events.CANCEL_UPLOAD);

		Assertions.assertThat(stateMachine.getState().getId()).isEqualTo(States.START);
	}

	@Test
	public void testNewSM() {
		StateMachine<States, Events> stateMachineOne = stateMachineFactory.getStateMachine("SM_ONE");
		StateMachine<States, Events> stateMachineTwo = stateMachineFactory.getStateMachine("SM_TWO");
		Assertions.assertThat(stateMachineOne).isNotEqualTo(stateMachineTwo);
	}

	@Test
	public void testTwoSM() {
		StateMachine<States, Events> stateMachineOne = stateMachineFactory.getStateMachine("SM_ONE");
		StateMachine<States, Events> stateMachineTwo = stateMachineFactory.getStateMachine("SM_TWO");
		stateMachineOne.sendEvent(Events.REGISTER);
		stateMachineOne.sendEvent(Events.UPLOADING);
		stateMachineOne.sendEvent(Events.FINISH);

		Assertions.assertThat(stateMachineOne.getState().getId()).isEqualTo(States.END);
		Assertions.assertThat(stateMachineTwo.getState().getId()).isEqualTo(States.START);
	}

}
