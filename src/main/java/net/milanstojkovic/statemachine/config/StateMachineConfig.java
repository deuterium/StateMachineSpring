package net.milanstojkovic.statemachine.config;

import net.milanstojkovic.statemachine.enums.Events;
import net.milanstojkovic.statemachine.enums.States;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

import java.util.EnumSet;

@EnableStateMachineFactory
@Configuration
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<States,Events> {

    @Override
    public void configure(StateMachineConfigurationConfigurer<States, Events> config) throws Exception {

        StateMachineListenerAdapter<States, Events> listenerAdapter = new StateMachineListenerAdapter<States, Events>(){
            @Override
            public void stateChanged(State<States, Events> from, State<States, Events> to) {
                System.out.println("State changed to: " + to.getId().toString());
            }
        };

        config
                .withConfiguration()
                .autoStartup(true)
                .listener(listenerAdapter);
    }

    @Override
    public void configure(StateMachineStateConfigurer<States, Events> states) throws Exception {

        states
                .withStates()
                .initial(States.START)
//                .end(States.END)
                .states(EnumSet.allOf(States.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<States, Events> transitions) throws Exception {

        transitions
                .withExternal()
                    .source(States.START)
                    .target(States.REGISTRED)
                    .event(Events.REGISTER)
                    .and()
                .withExternal()
                    .source(States.REGISTRED)
                    .target(States.UPLOADED)
                    .event(Events.UPLOADING)
                    .and()
                .withExternal()
                    .source(States.START)
                    .target(States.REGISTRED)
                    .event(Events.REGISTER)
                    .and()
                .withExternal()
                    .source(States.UPLOADED)
                    .target(States.END)
                    .event(Events.FINISH)
                    .and()
                .withExternal()
                    .source(States.REGISTRED)
                    .target(States.START)
                    .event(Events.CANCEL_REGISTRATION)
                    .and()
                .withExternal()
                    .source(States.UPLOADED)
                    .target(States.START)
                    .event(Events.CANCEL_UPLOAD);
    }
}
