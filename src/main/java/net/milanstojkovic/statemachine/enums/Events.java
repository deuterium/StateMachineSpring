package net.milanstojkovic.statemachine.enums;

public enum Events {
    REGISTER,
    UPLOADING,
    FINISH,
    CANCEL_REGISTRATION,
    CANCEL_UPLOAD
}
